package BuzzWord;

import SKEWordGameFramework.appData;
import SKEWordGameFramework.appGUI;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Time;
import java.util.*;


public class BuzzWordGUI extends appGUI
{
    Stage primaryStage;
    Scene scene;
    appData gameData = new appData();
    ArrayList<List> temp;
    ArrayList<String> levelWords = new ArrayList<>();
    ArrayList<Button> dragged = new ArrayList<>();
    Timeline timeline = new Timeline();
    Timeline checker = new Timeline();
    private static final int STARTTIME = 40;
    private IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
    boolean isDragging = false;
    boolean gameScreen = false;
    boolean isTyping = false;
    boolean found = false;

    @Override
    public void start(Stage primaryStage) throws Exception
    {

        getExit().setOnAction(e -> { exitGame();});
        setAlphabet();
        getTextDictionaryWords();



        getTopPane().getChildren().addAll(getExit());
        getTopPane().setRightAnchor(getExit(),3.0);

        getCenterPane().setAlignment(Pos.CENTER_LEFT);
        getCenterPane().getStyleClass().addAll("gridpane");
        getCenterPane().getColumnConstraints().addAll(new ColumnConstraints(70),new ColumnConstraints(70),new ColumnConstraints(70),new ColumnConstraints(70)); // column 0 is 100 wide
        getCenterPane().getRowConstraints().addAll(new RowConstraints(55),new RowConstraints(55),new RowConstraints(55),new RowConstraints(55)); // column 1 is 200 wide

        getBottomPane().setAlignment(Pos.TOP_CENTER);

        getBottomPane().getStyleClass().addAll("hbox2");
        getLeftPane().setAlignment(Pos.CENTER_LEFT);

        getLeftPane().getStyleClass().addAll("vbox");
        getRightPane().setAlignment(Pos.CENTER_RIGHT);
        getRightPane().getStyleClass().addAll("hbox");


        getBorderPane().setCenter(getCenterPane());
        getCenterPane().setHgap(20);
        getCenterPane().setVgap(20);
        getBorderPane().setLeft(getLeftPane());
        getBorderPane().setRight(getRightPane());
        getBorderPane().setTop(getTopPane());
        getBorderPane().setBottom(getBottomPane());

        getGameModeSelect().setValue("Dictionary");
        loadMenuScreen();


        /** Sets up scene and stage */
        scene = new Scene(getBorderPane(),1400,900);
        scene.getStylesheets().add(getClass().getResource("BuzzStyle.css").toExternalForm());
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            if(key.getCode() == KeyCode.ESCAPE) {
                exitGame();
            }
            if(key.getCode() == KeyCode.TAB)
            {
                if(gameScreen)
                    pauseHandler();
            }
            if(key.getCode() == KeyCode.SHIFT)
                showHelp();



        });

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setTitle("BuzzWord");
        primaryStage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void loadMenuScreen()
    {
        getLeftPane().getChildren().clear();
        getCenterPane().getChildren().clear();
        getBottomPane().getChildren().clear();
        getRightPane().getChildren().clear();
        getCenterPane().setAlignment(Pos.CENTER_LEFT);
        loadMenuGrid();
        if(gameData.getLoggedIn() == true)
        {
            getLogOut().setOnAction(e -> handleLogOut());
            getLevelSelect().setOnAction(e -> loadLevelSelectScreen());
            getGetUserName().setOnAction(e -> handleProfile());
            getHelp().setOnAction(e -> showHelp());
            getGetUserName().setDisable(false);
            Button gameModeSelect = new Button("Gamemode Select");
            gameModeSelect.setDisable(true);


            getLeftPane().getChildren().addAll(getGetUserName(),gameModeSelect,getGameModeSelect(),getLevelSelect(),getLogOut(),getHelp());
        }
        else
        {
            getLogIn().setOnAction(e -> handleLogIn());
            getCreateNewProfile().setOnAction(e -> handleCreateNewProfile());
            getLoad().setOnAction(e -> {
                try {
                    loadProfiles();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            });
            getLeftPane().getChildren().addAll(getLogIn(),getCreateNewProfile(),getLoad());
        }


    }
    public void loadMenuGrid()
    {
        for(int i = 0; i < 4; i++)
        {
            for(int j =0; j < 4; j++)
            {

                Button circle = new Button("");
                if(i == 0 && j==0)
                    circle = new Button("B");
                if(i == 0 && j==1)
                    circle = new Button("U");
                if(i == 1 && j==0)
                    circle = new Button("Z");
                if(i == 1 && j==1)
                    circle = new Button("Z");
                if(i == 2 && j==2)
                    circle = new Button("W");
                if(i == 2 && j==3)
                    circle = new Button("O");
                if(i == 3 && j==2)
                    circle = new Button("R");
                if(i == 3 && j==3)
                    circle = new Button("D");
                circle.setId("circle");
                getCenterPane().add(circle,j,i);
            }
        }
    }

    @Override
    public void loadLevelSelectScreen()
    {
        gameScreen = false;
        gameData.setIsPlaying(false);
        if(gameData.getPaused() == true)
            pauseHandler();
        gameData.setTotalScore(0);
        gameData.setTargetScore(0);
        getLeftPane().getChildren().clear();
        getRightPane().getChildren().clear();
        getBottomPane().getChildren().clear();
        getHome().setOnAction(e -> handleHome());
        getGetUserName().setDisable(true);
        getLeftPane().getChildren().addAll(getGetUserName(),getHome(),getLogOut(),getHelp());
        getCenterPane().getChildren().clear();
        getCenterPane().setAlignment(Pos.CENTER);

        Text gamemode = new Text(getGameModeSelect().getValue().toString());
        gamemode.getStyleClass().addAll("levelSelect");
        getBottomPane().getChildren().addAll(gamemode);


        Button level1 = new Button("1");
        level1.getStyleClass().addAll("circle");

        Button level2 = new Button("2");
        level2.getStyleClass().addAll("circle");
        level2.setDisable(true);

        Button level3 = new Button("3");
        level3.getStyleClass().addAll("circle");
        level3.setDisable(true);

        Button level4 = new Button("4");
        level4.getStyleClass().addAll("circle");
        level4.setDisable(true);

        Button level5 = new Button("5");
        level5.getStyleClass().addAll("circle");
        level5.setDisable(true);

        Button level6 = new Button("6");
        level6.getStyleClass().addAll("circle");
        level6.setDisable(true);

        Button level7 = new Button("7");
        level7.getStyleClass().addAll("circle");
        level7.setDisable(true);

        Button level8 = new Button("8");
        level8.getStyleClass().addAll("circle");
        level8.setDisable(true);

        if(getGameModeSelect().getValue().toString().equals("Dictionary"))
        {
            scene.getStylesheets().remove(getClass().getResource("BuzzStyle.css").toExternalForm());
            scene.getStylesheets().add(getClass().getResource("BuzzStyle.css").toExternalForm());
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(0) == 1)
            {
                level1.setDisable(false);
                level2.setDisable(true);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(0) == 2)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(0) == 3)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(0) == 4)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(false);
            }


        }
        if(getGameModeSelect().getValue().toString().equals("US Places"))
        {
            scene.getStylesheets().remove(getClass().getResource("deadStyle.css").toExternalForm());
            scene.getStylesheets().add(getClass().getResource("deadStyle.css").toExternalForm());
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(1) == 1)
            {
                level1.setDisable(false);
                level2.setDisable(true);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(1) == 2)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(1) == 3)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(1) == 4)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(false);
            }
        }
        if(getGameModeSelect().getValue().toString().equals("Names"))
        {
            scene.getStylesheets().remove(getClass().getResource("throne.css").toExternalForm());
            scene.getStylesheets().add(getClass().getResource("throne.css").toExternalForm());
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(2) == 1)
            {
                level1.setDisable(false);
                level2.setDisable(true);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(2) == 2)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(2) == 3)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(2) == 4)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(false);
            }
        }
        if(getGameModeSelect().getValue().toString().equals("Pokemon"))
        {
            scene.getStylesheets().remove(getClass().getResource("pokeStyle.css").toExternalForm());
            scene.getStylesheets().add(getClass().getResource("pokeStyle.css").toExternalForm());
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(3) == 1)
            {
                level1.setDisable(true);
                level2.setDisable(true);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(3) == 2)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(true);
                level4.setDisable(true);
            }
            if (gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(3) == 3)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(true);
            }
            if(gameData.getLevelsUnlocked().get(gameData.getUserIndex()).get(3) == 4)
            {
                level1.setDisable(false);
                level2.setDisable(false);
                level3.setDisable(false);
                level4.setDisable(false);
            }
        }

        getCenterPane().add(level1,0,0);
        getCenterPane().add(level2,1,0);
        getCenterPane().add(level3,2,0);
        getCenterPane().add(level4,3,0);

        level1.setOnAction(e -> {
            setLevel(1);
            loadGameplayScreen1();
        });
        level2.setOnAction(e -> {
            setLevel(2);
            loadGameplayScreen1();
        });
        level3.setOnAction(e -> {
            setLevel(3);
            loadGameplayScreen1();
        });
        level4.setOnAction(e -> {
            setLevel(4);
            loadGameplayScreen1();
        });
    }

    public void loadGameplayScreen1()
    {
        gameScreen = true;
        getBottomPane().getChildren().clear();
        gameData.setCurrentScore(0);
        getCorrectWordsPane().getChildren().clear();
        getCurrentProgressLabel().setText("");
        gameData.setCurrentScore(0);
        levelWords.clear();

        gameData.setIsPlaying(true);
        getLeftPane().getChildren().clear();
        getCenterPane().getChildren().clear();
        getRightPane().getChildren().clear();
        Text level = new Text (getGameModeSelect().getValue().toString() + " - Level " + Integer.toString(getLevel()));
        level.getStyleClass().addAll("levelSelect");
        getBottomPane().getChildren().addAll(level);

        loadGameGrid();

        //--------------Timer
        StackPane rectangleTime = new StackPane();
        Button timeButton = new Button();
        timeButton.setMinWidth((200));
        timeButton.setMinHeight(40);

        Label count = new Label();
        count.setText(timeSeconds.toString());
        count.setStyle("-fx-font-size: 25;");
        count.textProperty().bind(timeSeconds.asString());
        Label time = new Label("Time Left: ");
        time.setStyle("-fx-font-size: 25;");
        HBox timeHolder = new HBox(10);
        timeHolder.setAlignment(Pos.CENTER);
        timeHolder.getChildren().addAll(time,count);
        handleTimer();
        rectangleTime.getChildren().addAll( timeButton,timeHolder);
        //-------------------------------------PROGRESS


        StackPane rectCurrentWord = new StackPane();
        Button currentWordButton = new Button();
        currentWordButton.setMinWidth((200));
        currentWordButton.setMinHeight(40);
        rectCurrentWord.getChildren().addAll(currentWordButton,getCurrentProgressLabel());


        //------------------------------- SCROLL PANE
        VBox scrollHolder = new VBox(5);
        ScrollPane words = new ScrollPane();
        words.setMaxSize(180,180);
        words.setContent(getCorrectWordsPane());
        StackPane test = new StackPane();
        StackPane totalWord = new StackPane();
        Button rect3 = new Button();
        Button pane = new Button();
        test.getChildren().addAll(pane,words);
        pane.setMinWidth(200);
        pane.setMinHeight(200);
        rect3.setMinWidth((200));
        rect3.setMinHeight(40);
        totalWord.getChildren().addAll(rect3,getCurrentScore());
        scrollHolder.getChildren().addAll(test,totalWord);

        //-----------change

        StackPane targetPane= new StackPane();
        Button rect4 = new Button();
        rect4.setMinWidth((200));
        rect4.setMinHeight(40);
        Label tar = new Label("TARGET : " + gameData.getTargetScore());
        targetPane.getChildren().addAll(rect4,tar);
        getPause().setOnAction( e -> { pauseHandler();});
        getRightPane().getChildren().addAll(rectangleTime,rectCurrentWord, scrollHolder,targetPane);



        getNextLevel().setOnAction(e -> { nextLevel();});
        getReplayLevel().setOnAction(e -> { replayLevel();});
        getNextLevel().setDisable(true);
        getHome().setOnAction(e -> loadLevelSelectScreen());
        getLeftPane().getChildren().addAll(getGetUserName(),getHome(),getPause(),getHelp(),getReplayLevel(),getNextLevel());
    }

    public void loadGameGrid()
    {
        levelWords.clear();
        gameData.setGameEnded(false);
        char[][] letters = new char[4][4];
        getCenterPane().getChildren().clear();
        for(int i = 0; i < 4; i++)
        {
            for(int j =0; j < 4; j++)
            {
                int rand = (int) (Math.random() *104);
                Button circle = new Button(gameData.getAlphabet().get(rand));
                circle.setAccessibleText(gameData.getAlphabet().get(rand));
                circle.setId("circle");
                getCenterPane().add(circle,j,i);
                getCenterPane().setAlignment(Pos.CENTER);
                letters[i][j] = gameData.getAlphabet().get(rand).charAt(0);

                circle.setOnDragDetected(event -> {
                    if(gameData.isGameEnded() == false&& isTyping == false) {
                        isDragging = true;
                        circle.setMouseTransparent(true);
                        circle.startFullDrag();
                        dragged.add(circle);
                        circle.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(246, 8, 0, 0.6) , 5, 0.8 , 0 , 1 )");
                        getCurrentProgressLabel().setText(getCurrentProgressLabel().getText() + circle.getAccessibleText());
                        dragged.get(0).setMouseTransparent(false);
                    }
                });

                circle.setOnMouseDragEntered(event -> {
                    if(gameData.isGameEnded() == false&& isTyping == false) {
                        if (!dragged.contains(circle)) {
                            dragged.add(circle);
                            circle.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(246, 8, 0, 0.6) , 5, 0.8 , 0 , 1 )");
                            getCurrentProgressLabel().setText(getCurrentProgressLabel().getText() + circle.getAccessibleText());
                        } else {
                            if (!dragged.get(dragged.size() - 1).equals(circle)) {

                                dragged.get(dragged.size() - 1).setStyle("-fx-effect: dropshadow( three-pass-box , rgba(28, 0, 246, 0.6) , 5, 0.0 , 0 , 1 )");
                                String temp = getCurrentProgressLabel().getText().substring(0, getCurrentProgressLabel().getText().length() - 1);
                                getCurrentProgressLabel().setText(temp);
                                dragged.remove(dragged.get(dragged.size() - 1));
                            }
                        }
                    }
                });
                circle.setOnMouseDragReleased(e -> {
                    if(gameData.isGameEnded() == false && isTyping == false) {
                    handleMouseClickReleaseEvent();}});

                getCenterPane().setOnMouseDragEntered(e -> {

                });
                getCenterPane().setOnMouseDragReleased(e -> {
                    if(gameData.isGameEnded() == false && isTyping == false) {
                        handleMouseClickReleaseEvent();
                    }
                });

               scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                    if(isDragging == false && gameData.isGameEnded() == false)
                    {
                        isTyping = true;


                        if(circle.getAccessibleText().toLowerCase().equals(key.getText().toLowerCase()))
                        {

                            circle.setStyle("-fx-effect: dropshadow( three-pass-box , rgba(246, 8, 0, 0.6) , 5, 0.8 , 0 , 1 )");
                            getCurrentProgressLabel().setText(getCurrentProgressLabel().getText() + circle.getAccessibleText());

                        }

                        if(key.getCode() == KeyCode.ENTER) {
                            dragged.add(circle);
                            handleMouseClickReleaseEvent();
                        }


                    }
                });
            }

        }

        if(getGameModeSelect().getValue().toString().equals("Dictionary"))
        {
            for (int j = 1; j < gameData.getTextDictionary().size(); j++) {
                searchWord(letters, gameData.getTextDictionary().get(j).toUpperCase());
            }
        }
        if(getGameModeSelect().getValue().toString().equals("US Places"))
        {
            for (int j = 1; j < gameData.getTextPlaces().size(); j++) {
                searchWord(letters, gameData.getTextPlaces().get(j).toUpperCase());
            }
        }
        if(getGameModeSelect().getValue().toString().equals("Names"))
        {
            for (int j = 1; j < gameData.getTextNames().size(); j++) {
                searchWord(letters, gameData.getTextNames().get(j).toUpperCase());
            }
        }
        if(getGameModeSelect().getValue().toString().equals("Pokemon"))
        {
            for (int j = 1; j < gameData.getTextPokemon().size(); j++) {
                searchWord(letters, gameData.getTextPokemon().get(j).toUpperCase());
            }
        }



        for(int j = 0; j < levelWords.size(); j++){
            if(levelWords.get(j).length() == 3) {
                gameData.setTotalScore(gameData.getTotalScore() + 10);
                gameData.setTargetScore(gameData.getTargetScore() + 1);
            }
            if(levelWords.get(j).length() == 4) {
                gameData.setTotalScore(gameData.getTotalScore() + 20);
                gameData.setTargetScore(gameData.getTargetScore() + 2);
            }
            if(levelWords.get(j).length() == 5) {
                gameData.setTotalScore(gameData.getTotalScore() + 30);
                gameData.setTargetScore(gameData.getTargetScore() + 3);
            }
            if(levelWords.get(j).length() == 6) {
                gameData.setTotalScore(gameData.getTotalScore() + 40);
                gameData.setTargetScore(gameData.getTargetScore() + 4);
            }
            if(levelWords.get(j).length() >= 7) {
                gameData.setTotalScore(gameData.getTotalScore() + 60);
                gameData.setTargetScore(gameData.getTargetScore() + 5);
            }

        }

        getCurrentScore().setText("Current Score: ");
        if(getLevel() == 1){
            gameData.setTargetScore(gameData.getTargetScore()/20);
            if(getGameModeSelect().getValue().toString().equals("Pokemon") || getGameModeSelect().getValue().toString().equals("US Places"))
                gameData.setTargetScore(10);
        }

        if(getLevel() == 2)
        {
            gameData.setTargetScore(gameData.getTargetScore()/18);
            if(getGameModeSelect().getValue().toString().equals("Pokemon") || getGameModeSelect().getValue().toString().equals("US Places"))
                gameData.setTargetScore(20);
        }

        if(getLevel() == 3)
        {
            gameData.setTargetScore(gameData.getTargetScore()/16);
            if(getGameModeSelect().getValue().toString().equals("Pokemon") || getGameModeSelect().getValue().toString().equals("US Places"))
                gameData.setTargetScore(30);
        }

        if(getLevel() == 4)
        {
            gameData.setTargetScore(gameData.getTargetScore()/14);
            if(getGameModeSelect().getValue().toString().equals("Pokemon") || getGameModeSelect().getValue().toString().equals("US Places"))
                gameData.setTargetScore(40);
        }

    }

    public void setAlphabet()
    {
      for(int i = 0; i < 8;i ++)
      {
          gameData.getAlphabet().add("A");
      }
        for(int i = 0; i < 2;i ++)
        {
            gameData.getAlphabet().add("B");
        }
        for(int i = 0; i < 3;i ++)
        {
            gameData.getAlphabet().add("C");
        }
        for(int i = 0; i < 4;i ++)
        {
            gameData.getAlphabet().add("D");
        }
        for(int i = 0; i < 13;i ++)
        {
            gameData.getAlphabet().add("E");
        }
        for(int i = 0; i < 2;i ++)
        {
            gameData.getAlphabet().add("F");
        }
        for(int i = 0; i < 2;i ++)
        {
            gameData.getAlphabet().add("G");
        }
        for(int i = 0; i < 6;i ++)
        {
            gameData.getAlphabet().add("H");
        }
        for(int i = 0; i < 7;i ++)
        {
            gameData.getAlphabet().add("I");
        }
        for(int i = 0; i < 1;i ++)
        {
            gameData.getAlphabet().add("J");
        }
        for(int i = 0; i < 1;i ++)
        {
            gameData.getAlphabet().add("K");
        }
        for(int i = 0; i < 4;i ++)
        {
            gameData.getAlphabet().add("L");
        }
        for(int i = 0; i < 3;i ++)
        {
            gameData.getAlphabet().add("M");
        }
        for(int i = 0; i < 7;i ++)
        {
            gameData.getAlphabet().add("N");
        }
        for(int i = 0; i < 8;i ++)
        {
            gameData.getAlphabet().add("O");
        }
        for(int i = 0; i < 2;i ++)
        {
            gameData.getAlphabet().add("P");
        }
        for(int i = 0; i < 1;i ++)
        {
            gameData.getAlphabet().add("Q");
        }
        for(int i = 0; i < 6;i ++)
        {
            gameData.getAlphabet().add("R");
        }
        for(int i = 0; i < 6;i ++)
        {
            gameData.getAlphabet().add("S");
        }
        for(int i = 0; i < 9;i ++)
        {
            gameData.getAlphabet().add("T");
        }
        for(int i = 0; i < 3;i ++)
        {
            gameData.getAlphabet().add("U");
        }
        for(int i = 0; i < 1;i ++)
        {
            gameData.getAlphabet().add("V");
        }
        for(int i = 0; i < 2;i ++)
        {
            gameData.getAlphabet().add("W");
        }
        for(int i = 0; i < 1;i ++)
        {
            gameData.getAlphabet().add("X");
        }
        for(int i = 0; i < 2;i ++)
        {
            gameData.getAlphabet().add("Y");
        }
        for(int i = 0; i < 1;i ++)
        {
            gameData.getAlphabet().add("Z");
        }

        Collections.shuffle(gameData.getAlphabet());
    }

    public void handleLogIn()
    {
        Button accept = new Button("Enter");
        final Stage logIn = new Stage();
        logIn.initModality(Modality.APPLICATION_MODAL);
        logIn.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);
        HBox userName = new HBox(10);
        HBox passWord = new HBox(10);
        userName.setAlignment(Pos.CENTER);
        passWord.setAlignment(Pos.CENTER);
        Label password = new Label("Password");
        Label user = new Label("Username");
        PasswordField passwordText = new PasswordField();
        TextField username = new TextField("username");
        userName.getChildren().addAll(user,username);
        passWord.getChildren().addAll(password, passwordText);
        layoutVbox.getChildren().addAll(userName,passWord,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        logIn.setResizable(false);
        logIn.setTitle("Log In");
        logIn.setScene(popupScene);
        logIn.show();

        accept.setOnAction(e -> {
            boolean userCorrect = false;
            boolean passCorrect = false;
            if(gameData.getUserNames().size() > 0)
            {
                for (int i = 0; i < gameData.getUserNames().size(); i++) {
                    if (gameData.getUserNames().get(i).equals(username.getText())) {
                        gameData.setUserIndex(i);
                        userCorrect = true;
                    }
                }

                if (gameData.getPasswords().get(gameData.getUserIndex()).equals(passwordText.getText())) {
                    passCorrect = true;
                }
            }
            if(userCorrect && passCorrect)
            {
                gameData.setLoggedIn(true);
                setGetUserName(new Button(gameData.getUserNames().get(gameData.getUserIndex())));
                logIn.close();
                loadMenuScreen();
            }
            else
            {

                if(layoutVbox.getChildren().contains(getInvalid()))
                {
                    layoutVbox.getChildren().remove(getInvalid());
                }
                layoutVbox.getChildren().add(getInvalid());
            }


        });
    }

    public void handleCreateNewProfile()
    {
        Button accept = new Button("Enter");
        final Stage newProfile = new Stage();
        newProfile.initModality(Modality.APPLICATION_MODAL);
        newProfile.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);
        HBox userName = new HBox(10);
        HBox passWord = new HBox(10);
        userName.setAlignment(Pos.CENTER);
        passWord.setAlignment(Pos.CENTER);
        Label password = new Label("Password");
        Label user = new Label("Username");
        TextField passwordText = new PasswordField();
        TextField username = new TextField("username");
        userName.getChildren().addAll(user,username);
        passWord.getChildren().addAll(password, passwordText);
        layoutVbox.getChildren().addAll(userName,passWord,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        newProfile.setScene(popupScene);
        newProfile.setResizable(false);
        newProfile.setTitle("Create Profile");
        newProfile.show();

        accept.setOnAction(e -> {

            gameData.getUserNames().add(username.getText());
            gameData.getPasswords().add(passwordText.getText());
            gameData.getLevelsUnlocked().add(new ArrayList<Integer>());
            gameData.getLevelsUnlocked().get(gameData.getUsersCreated()).addAll(Arrays.asList(1,1,1,1));
            gameData.getHighscore().add(new ArrayList<Integer>());
            gameData.getHighscore().get(gameData.getUsersCreated()).addAll(Arrays.asList(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0));
            gameData.setUsersCreated(gameData.getUsersCreated() + 1);
            newProfile.close();

        });
    }

    public void handleLogOut()
    {

        gameData.setLoggedIn(false);
        loadMenuScreen();
    }

    public void handleHome()
    {
        loadMenuScreen();
    }

    public void handleProfile()
    {
        Button accept = new Button("Enter");
        final Stage logIn = new Stage();
        logIn.initModality(Modality.APPLICATION_MODAL);
        logIn.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);
        HBox userName = new HBox(10);
        HBox passWord = new HBox(10);
        userName.setAlignment(Pos.CENTER);
        passWord.setAlignment(Pos.CENTER);
        Label password = new Label(" New Password");
        Label user = new Label("New Username");
        PasswordField passwordText = new PasswordField();
        TextField username = new TextField("new username");
        userName.getChildren().addAll(user,username);
        passWord.getChildren().addAll(password, passwordText);
        layoutVbox.getChildren().addAll(userName,passWord,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        logIn.setResizable(false);
        logIn.setTitle("Edit Profile");
        logIn.setScene(popupScene);
        logIn.show();

        accept.setOnAction(e -> {
            gameData.getUserNames().set(gameData.getUserIndex(),username.getText());
            gameData.getPasswords().set(gameData.getUserIndex(),passwordText.getText());
            setGetUserName(new Button(gameData.getUserNames().get(gameData.getUserIndex())));
            logIn.close();
            loadMenuScreen();
        });
    }

    public void showHelp()
    {
        Button accept = new Button("Continue");
        final Stage logIn = new Stage();
        logIn.initModality(Modality.APPLICATION_MODAL);
        logIn.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);

        Label quote = new Label("Help Menu!");

        ScrollPane words = new ScrollPane();
        VBox correcto = new VBox();
        words.setMaxSize(200,200);
        Label temp = new Label("Pick the Mode!");
        Label temp1 = new Label("Pick the Level!");
        Label temp2 = new Label("Guess the words!");
        Label temp3 = new Label("Its that!");
        Label temp4 = new Label("EASY!");
        correcto.getChildren().addAll(temp,temp1,temp2,temp3,temp4);


        words.setContent(correcto);
        layoutVbox.getChildren().addAll(quote,words,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        logIn.setResizable(false);
        logIn.setTitle("Help");
        logIn.setScene(popupScene);
        logIn.show();

        accept.setOnAction(e -> {

            logIn.close();

        });
    }

    public void exitGame()
    {
        if(gameData.getIsPlaying()) {
            gameData.setPaused(false);
            pauseHandler();
        }
        Button accept = new Button("Continue");
        final Stage exit = new Stage();
        exit.initModality(Modality.APPLICATION_MODAL);
        exit.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);

        Label quote = new Label("ARE YOU SURE?");

        layoutVbox.getChildren().addAll(quote,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        exit.setResizable(false);
        exit.setTitle("EXIT");
        exit.setScene(popupScene);
        exit.show();

        accept.setOnAction(e -> {

            save();
            exit.close();
            System.exit(1);

        });
    }

    public void save()
    {
        try {
            gameData.saveGame(primaryStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadProfiles() throws IOException,NullPointerException {

        ObjectInputStream ois = null;
        FileInputStream fis = null;
        try {

            fis = new FileInputStream(gameData.loadGame(primaryStage));
            if(gameData.getInitial() != null) {

                ois = new ObjectInputStream(fis);
                temp = (ArrayList<List>) ois.readObject();
                 for(int i = 0; i < temp.get(0).size(); i++){
                     gameData.getUserNames().add(i,(String)temp.get(0).get(i));
                     gameData.getPasswords().add(i,(String)temp.get(1).get(i));

                     gameData.setUsersCreated(i+1);
                 }
                for(int i = 0; i < temp.get(2).size(); i++){
                    gameData.getLevelsUnlocked().add(i, (ArrayList<Integer>) temp.get(2).get(i));
                }
                for(int i = 0; i < temp.get(3).size(); i++){
                    gameData.getHighscore().add(i, (ArrayList<Integer>) temp.get(3).get(i));
                }


            }
        } catch (Exception e) {
            System.out.println("Null");
        } finally {
            if(ois != null){
                ois.close();
            }
        }

    }



    public void getTextDictionaryWords() throws FileNotFoundException {
        FileInputStream file = new FileInputStream("BuzzWord/Resources/Texts/dictionary.txt");
        Scanner scanner = new Scanner(file);
        while(scanner.hasNext())
        {
            gameData.getTextDictionary().add(scanner.next().toLowerCase());
        }
        file = new FileInputStream("BuzzWord/Resources/Texts/places.txt");
        scanner = new Scanner(file);
        while(scanner.hasNext())
        {
            gameData.getTextPlaces().add(scanner.next().toLowerCase());
        }
        file = new FileInputStream("BuzzWord/Resources/Texts/names.txt");
        scanner = new Scanner(file);
        while(scanner.hasNext())
        {
            gameData.getTextNames().add(scanner.next().toLowerCase());
        }
        file = new FileInputStream("BuzzWord/Resources/Texts/Pokemon.txt");
        scanner = new Scanner(file);
        while(scanner.hasNext())
        {
            gameData.getTextPokemon().add(scanner.next().toLowerCase());
        }


    }


    public boolean searchWord(char[][] matrix, String word) {
        int size = matrix.length;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j <size; j++) {
                if (search(matrix, word, i, j, 0, size)) {
                    if(levelWords.contains(word))
                    {
                        System.out.println("no" + word);
                        return false;
                    }
                    else {
                        if(word.length() > 2) {
                            levelWords.add(word);
                            System.out.println(word);
                            return true;
                        }
                    }

                }

            }
        }

        return false;
    }

    public boolean search(char[][] matrix, String word, int row, int col,
                          int index, int N) {

        // check if current cell not already used or character in it is not not

        if (word.charAt(index) != matrix[row][col]) {
            return false;
        }

        if (index == word.length() - 1) {
            // word is found, return true

            return true;
        }

        // mark the current cell as 1

        // check if cell is already used

        if (row + 1 < N && search(matrix, word, row + 1, col, index + 1, N)) { // go
            // down
            return true;
        }
        if (row - 1 >= 0 && search(matrix, word, row - 1, col, index + 1, N)) { // go
            // up
            return true;
        }
        if (col + 1 < N && search(matrix, word, row, col + 1, index + 1, N)) { // go
            // right
            return true;
        }
        if (col - 1 >= 0 && search(matrix, word, row, col - 1, index + 1, N)) { // go
            // left
            return true;
        }
        if (row - 1 >= 0 && col + 1 < N
                && search(matrix, word, row - 1, col + 1, index + 1, N)) {
            // go diagonally up right
            return true;
        }
        if (row - 1 >= 0 && col - 1 >= 0
                && search(matrix, word, row - 1, col - 1, index + 1, N)) {
            // go diagonally up left
            return true;
        }
        if (row + 1 < N && col - 1 >= 0
                && search(matrix, word, row + 1, col - 1, index + 1, N)) {
            // go diagonally down left
            return true;
        }
        if (row + 1 < N && col + 1 < N
                && search(matrix, word, row + 1, col + 1, index + 1, N)) {
            // go diagonally down right
            return true;
        }

        // if none of the option works out, BACKTRACK and return false

        return false;
    }

    public void pauseHandler()
    {
        GridPane temp =  new GridPane();
        Label pauseLabel = new Label("PAUSED");
        temp.setAlignment(Pos.CENTER_RIGHT);
        temp.getStyleClass().addAll("gridpane");
        temp.add(pauseLabel,0,0);
        if(gameData.getPaused() == true)
        {
            gameData.setPaused(false);
            gameData.setIsPlaying(true);
            getBorderPane().getChildren().remove(temp);
            getBorderPane().setCenter(getCenterPane());
            timeline.play();
            return;
        }
        if(gameData.getPaused() == false)
        {
            getBorderPane().getChildren().remove(getCenterPane());
            getBorderPane().setCenter(temp);
            gameData.setPaused(true);
            gameData.setIsPlaying(false);
            timeline.pause();
            return;
        }

    }

    public void handleTimer()
    {
        if (timeline != null) {
            timeline.stop();
        }


        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(STARTTIME+1),
                        new KeyValue(timeSeconds, 0)));

        timeline.playFromStart();


        Timeline checker = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {

                if (gameData.getIsPlaying() == true && (timeSeconds.intValue() == 0)) {
                        gameData.setGameEnded(true);

                    if(gameData.getCurrentScore() >= gameData.getTargetScore())
                    {
                        if(getGameModeSelect().getValue().toString().equals("Dictionary") && getLevel() <= 4)
                        {
                            if(gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1) < gameData.getCurrentScore())
                                gameData.getHighscore().get(gameData.getUserIndex()).set(getLevel()-1,gameData.getCurrentScore());

                            if(getLevel() < 4) {
                                gameData.getLevelsUnlocked().get(gameData.getUserIndex()).set(0, getLevel() + 1);
                                getNextLevel().setDisable(false);
                            }
                            gameData.setIsPlaying(false);
                            endLevelScreen();

                        }
                        if(getGameModeSelect().getValue().toString().equals("US Places") && getLevel() <= 4)
                        {
                            if(gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1 + 4) < gameData.getCurrentScore())
                                gameData.getHighscore().get(gameData.getUserIndex()).set(getLevel()-1 + 4,gameData.getCurrentScore());

                            if(getLevel() < 4) {
                                gameData.getLevelsUnlocked().get(gameData.getUserIndex()).set(1, getLevel() + 1);
                                getNextLevel().setDisable(false);
                            }
                            gameData.setIsPlaying(false);
                            endLevelScreen();
                        }
                        if(getGameModeSelect().getValue().toString().equals("Names") && getLevel() <= 4)
                        {
                            if(gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1 + 8) < gameData.getCurrentScore())
                                gameData.getHighscore().get(gameData.getUserIndex()).set(getLevel()-1 + 8,gameData.getCurrentScore());

                            if(getLevel() < 4) {
                                gameData.getLevelsUnlocked().get(gameData.getUserIndex()).set(2, getLevel() + 1);
                                getNextLevel().setDisable(false);
                            }
                            gameData.setIsPlaying(false);
                            endLevelScreen();
                        }
                        if(getGameModeSelect().getValue().toString().equals("Pokemon") && getLevel() <= 4)
                        {
                            if(gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1 + 12) < gameData.getCurrentScore())
                                gameData.getHighscore().get(gameData.getUserIndex()).set(getLevel()-1 + 12,gameData.getCurrentScore());

                            if(getLevel() < 4) {
                                gameData.getLevelsUnlocked().get(gameData.getUserIndex()).set(3, getLevel() + 1);
                                getNextLevel().setDisable(false);
                            }
                            gameData.setIsPlaying(false);
                            endLevelScreen();
                        }
                    }
                    else {
                        gameData.setIsPlaying(false);
                        endLevelLostScreen();
                    }
                }


            }
        }));
        checker.setCycleCount(Timeline.INDEFINITE);
        checker.play();
    }

    public void handleMouseClickReleaseEvent()
    {
        found = false;
        isDragging = false;
        isTyping = false;
        Label correctWord = new Label();
        if(levelWords.contains(getCurrentProgressLabel().getText().toUpperCase()))
        {
            if(getCurrentProgressLabel().getText().length() == 3) {
                gameData.setCurrentScore(gameData.getCurrentScore() + 10);
                correctWord = new Label(getCurrentProgressLabel().getText().toUpperCase()+ " 10");
            }
            if(getCurrentProgressLabel().getText().length() == 4) {
                gameData.setCurrentScore(gameData.getCurrentScore() + 20);
                correctWord = new Label(getCurrentProgressLabel().getText().toUpperCase()+ " 20");
            }
            if(getCurrentProgressLabel().getText().length() == 5) {
                gameData.setCurrentScore(gameData.getCurrentScore() + 30);
                correctWord = new Label(getCurrentProgressLabel().getText().toUpperCase()+ " 30");
            }
            if(getCurrentProgressLabel().getText().length() == 6) {
                gameData.setCurrentScore(gameData.getCurrentScore() + 40);
                correctWord = new Label(getCurrentProgressLabel().getText().toUpperCase()+ " 40");
            }
            if(getCurrentProgressLabel().getText().length() >= 7) {
                gameData.setCurrentScore(gameData.getCurrentScore() + 60);
                correctWord = new Label(getCurrentProgressLabel().getText().toUpperCase()+ " 60");
            }

            getCurrentScore().setText("Current Score: " + gameData.getCurrentScore());

            getCorrectWordsPane().getChildren().addAll(correctWord);
            levelWords.remove(getCurrentProgressLabel().getText().toUpperCase());
        }
        dragged.get(0).setMouseTransparent(false);
        for(int i = 0; i < dragged.size(); i++)
        {
            dragged.get(i).setStyle("-fx-effect: dropshadow( three-pass-box , rgba(28, 0, 246, 0.6) , 5, 0.0 , 0 , 1 )");
            getCurrentProgressLabel().setText("");
        }
        dragged.clear();
    }

    public void nextLevel(){
        getCurrentScore().setText("Current Score: ");
        setLevel(getLevel()+1);
        loadGameplayScreen1();
    }
    public void replayLevel(){
        if(!gameData.getIsPlaying() && !gameData.isGameEnded())
            pauseHandler();
        getCurrentScore().setText("Current Score: ");
        setLevel(getLevel());
        loadGameplayScreen1();
    }

    public void endLevelScreen()
    {
        Button accept = new Button("Continue");
        final Stage endLevel = new Stage();
        endLevel.initModality(Modality.APPLICATION_MODAL);
        endLevel.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);

        Label win = new Label("Winner!");
        Label quote = new Label();
        if(getGameModeSelect().getValue().toString().equals("Dictionary"))
        {
             quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1) +" !");
        }
        if(getGameModeSelect().getValue().toString().equals("US Places"))
        {
            quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1+4) +" !");
        }
        if(getGameModeSelect().getValue().toString().equals("Names"))
        {
            quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1+8) +" !");
        }
        if(getGameModeSelect().getValue().toString().equals("Pokemon"))
        {
             quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1 + 12) +" !");
        }

        ScrollPane words = new ScrollPane();
        VBox correcto = new VBox();
        words.setMaxSize(180,180);
        for(int i = 0; i < levelWords.size(); i++){
            Label temp = new Label(levelWords.get(i));
            correcto.getChildren().addAll(temp);
        }
        words.setContent(correcto);
        layoutVbox.getChildren().addAll(win,quote,words,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        endLevel.setResizable(false);
        endLevel.setTitle("Level Ended");
        endLevel.setScene(popupScene);
        endLevel.show();

        accept.setOnAction(e -> {

            endLevel.close();

        });
    }

    public void endLevelLostScreen()
    {
        Button accept = new Button("Continue");
        final Stage endLevel = new Stage();
        endLevel.initModality(Modality.APPLICATION_MODAL);
        endLevel.initOwner(primaryStage);
        VBox layoutVbox = new VBox(20);
        layoutVbox.setAlignment(Pos.CENTER);

        Label lose = new Label("LOSER!");
        Label quote = new Label();
        if(getGameModeSelect().getValue().toString().equals("Dictionary"))
        {
            quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1) +" !");
        }
        if(getGameModeSelect().getValue().toString().equals("US Places"))
        {
            quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1+4) +" !");
        }
        if(getGameModeSelect().getValue().toString().equals("Names"))
        {
            quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1+8) +" !");
        }
        if(getGameModeSelect().getValue().toString().equals("Pokemon"))
        {
            quote = new Label("HIGH SCORE IS: " + gameData.getHighscore().get(gameData.getUserIndex()).get(getLevel()-1 + 12) +" !");
        }

        ScrollPane words = new ScrollPane();
        VBox correcto = new VBox();
        words.setMaxSize(180,180);
        for(int i = 0; i < levelWords.size(); i++){
            Label temp = new Label(levelWords.get(i));
            correcto.getChildren().addAll(temp);
        }
        words.setContent(correcto);
        layoutVbox.getChildren().addAll(lose,quote,words,accept);
        Scene popupScene = new Scene(layoutVbox, 300, 200);
        popupScene.getStylesheets().add(getClass().getResource("popStyle.css").toExternalForm());
        endLevel.setResizable(false);
        endLevel.setTitle("Level Ended");
        endLevel.setScene(popupScene);
        endLevel.show();

        accept.setOnAction(e -> {

            endLevel.close();

        });
    }

}




