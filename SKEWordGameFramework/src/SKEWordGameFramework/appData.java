package SKEWordGameFramework;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 11/12/2016.
 */
public class appData implements Serializable {

    String initial ;

    private boolean paused = false;
    private boolean loggedIn = false;
    private boolean pickedGameMode = false;
    private boolean isPlaying = false;
    private boolean gameEnded = false;



    private ArrayList<String> passwords = new ArrayList<>();
    private ArrayList<String> usernames = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> levelsUnlocked = new ArrayList<>();
    private ArrayList<String> alphabet = new ArrayList<>();
    private ArrayList<String> textDictionary = new ArrayList<>();
    private ArrayList<String> textPlaces = new ArrayList<>();
    private ArrayList<String> textNames = new ArrayList<>();
    private ArrayList<String> textPokemon = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> highscore = new ArrayList<>();

    private int userIndex = 0;
    private int usersCreated = 0;
    private int targetScore;
    private int totalScore;
    private int currentScore;
    private String currentLevel = "1";

    public ArrayList<ArrayList<Integer>> getHighscore() { return highscore;}
    public String getCurrentLevel() { return currentLevel;}
    public void setCurrentLevel(String currentLevel) { this.currentLevel = currentLevel;}

    public int getCurrentScore() { return  currentScore;}
    public void setCurrentScore(int currentScore) { this .currentScore = currentScore;}

    public boolean isGameEnded(){return gameEnded;}
    public void setGameEnded(boolean gameEnded) { this.gameEnded = gameEnded;}

    public boolean getIsPlaying() { return isPlaying;}
    public void setIsPlaying(boolean isPlaying) { this.isPlaying = isPlaying;}


    public void setPaused(boolean paused) { this.paused = paused;}
    public boolean getPaused(){ return paused;}

    public void setTargetScore(int targetScore) { this.targetScore = targetScore;}
    public void setTotalScore(int totalScore) { this.totalScore = totalScore;}

    public int getTargetScore(){return targetScore;}
    public int getTotalScore() { return totalScore;}

    public ArrayList<String> getTextDictionary () { return textDictionary;}
    public ArrayList<String> getTextPlaces () { return textPlaces;}
    public ArrayList<String> getTextNames () { return textNames;}
    public ArrayList<String> getTextPokemon () { return textPokemon;}

    public ArrayList<String> getAlphabet () { return alphabet;}

    public boolean getLoggedIn() {
        return this.loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public ArrayList<String> getUserNames() {
        return usernames;
    }

    public ArrayList<String> getPasswords() {
        return passwords;
    }

    public ArrayList<ArrayList<Integer>> getLevelsUnlocked() {
        return levelsUnlocked;
    }

    public boolean getPickedGameMode() {
        return pickedGameMode;
    }

    public void setPickedGameMode(Boolean pickedGameMode) {
        this.pickedGameMode = pickedGameMode;
    }

    public int getUserIndex() {
        return userIndex;
    }

    public void setUserIndex(int userIndex) {
        this.userIndex = userIndex;
    }

    public int getUsersCreated() {
        return usersCreated;
    }

    public void setUsersCreated(int usersCreated) {
        this.usersCreated = usersCreated;
    }

    public void saveGame(Stage mainStage) throws IOException {

        boolean dir = false;
        ArrayList<String> passwordsTemp = getPasswords();
        ArrayList<String> usernamesTemp = getUserNames();
        ArrayList<ArrayList<Integer>> levelsUnlockedTemp = getLevelsUnlocked();
        ArrayList<ArrayList<Integer>> highscoreTemp = getHighscore();
        ArrayList<List> temp = new ArrayList<List>();

        temp.add(usernamesTemp);
        temp.add(passwordsTemp);
        temp.add(levelsUnlockedTemp);
        temp.add(highscoreTemp);

        DirectoryChooser dChooser = new DirectoryChooser();
        FileChooser chooser = new FileChooser();
        chooser.setTitle("SAVE");
        dChooser.setTitle("SAVE");
        File selectedDirectory;
        if(initial != null)
        {
            File defaultDirectory = new File(initial);
            chooser.setInitialDirectory(defaultDirectory);
            FileChooser.ExtensionFilter extFilter =
                    new FileChooser.ExtensionFilter("Buzzword", "*.ser");
            chooser.getExtensionFilters().add(extFilter);
           selectedDirectory = chooser.showOpenDialog(mainStage);

        }
        else
        {
            selectedDirectory = dChooser.showDialog(mainStage);
            dir = true;
        }

        if(selectedDirectory!=null)
        {
            if(dir) {
                FileOutputStream fos = new FileOutputStream(selectedDirectory + "/BuzzWord.ser");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(temp);
                oos.close();
                initial = selectedDirectory.getAbsolutePath();
            }
            else
            {
                FileOutputStream fos = new FileOutputStream(selectedDirectory);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(temp);
                oos.close();

            }

        }



    }

    public File loadGame(Stage mainStage)
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("SAVE");
        File selectedDirectory;
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("Buzzword", "*.ser");
        chooser.getExtensionFilters().add(extFilter);

         selectedDirectory = chooser.showOpenDialog(mainStage);
        if(selectedDirectory!=null)
        {
            initial = selectedDirectory.getAbsolutePath();
            initial = initial.replace("BuzzWord.ser","");
        }

        return selectedDirectory;
    }

    public String getInitial() { return initial;}



}
