package SKEWordGameFramework;

import java.io.IOException;

/**
 * Created by User on 11/12/2016.
 */
public interface appController {


        void handleNewRequest();

        void handleSaveRequest() throws IOException;

        void handleLoadRequest() throws IOException;

        void handleIconRequest() throws IOException;

        void handleExitRequest();

        void handleLoadMenuScene();

        void handleLoadLevelSelectScene();
}
