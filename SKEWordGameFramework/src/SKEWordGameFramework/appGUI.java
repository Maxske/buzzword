package SKEWordGameFramework;

import BuzzWord.BuzzWordController;
import BuzzWord.BuzzWordGUI;
import SKEWordGameFramework.appController;
import com.sun.corba.se.spi.ior.ObjectKey;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public abstract class  appGUI extends Application {


   // private Pane Layout = new Pane();
    private BorderPane borderPane = new BorderPane();
    private VBox leftPane = new VBox(20);
    private VBox rightPane = new VBox();
    private AnchorPane topPane = new AnchorPane();
    private HBox bottomPane = new HBox();
    private GridPane centerPane = new GridPane();
    private VBox correctWordsPane = new VBox();


    private Button exit = new Button("X");
    private Button logIn = new Button("Log In");
    private Button createNewProfile = new Button(("Create New Profile"));
    private Button logOut = new Button("Log Out");
    private Button levelSelect = new Button("Select Level");
    private ChoiceBox gameModeSelect = new ChoiceBox(FXCollections.observableArrayList(
            "Dictionary", "US Places","Names", "Pokemon"));
    private Button getUserName = new Button("USERNAME HERE");
    private Button help = new Button("Help");
    private Button home = new Button("Home");
    private Button pause = new Button ("Pause");
    private Button load = new Button("Load Profiles");
    private Button nextLevel = new Button("Next Level");
    private Button replayLevel = new Button("Replay Level");


    private Label invalid = new Label("INVALID COMBINATION");
    private Label currentProgress = new Label("");
    private Label currentScore = new Label("Current Score: ");

    private int level = 1;

 public Button getNextLevel () { return nextLevel;}
 public Button getReplayLevel () { return replayLevel;}
    public Label getCurrentScore() { return currentScore;}
    public Label getCurrentProgressLabel() {return currentProgress;}
    public BorderPane getBorderPane () { return borderPane;}
    public VBox getLeftPane() { return leftPane;}
    public VBox getRightPane() { return rightPane;}
    public AnchorPane getTopPane () { return topPane;}
    public HBox getBottomPane () { return bottomPane;}
    public GridPane getCenterPane () { return  centerPane;}

    public VBox getCorrectWordsPane() { return correctWordsPane;}
    public Button getCreateNewProfile() { return createNewProfile;}
    public Button getLogIn() { return logIn;}
    public Button getLogOut () { return logOut;}
    public Button getExit() { return exit;}
    public Button getLevelSelect() { return levelSelect;}
    public ChoiceBox getGameModeSelect() { return gameModeSelect;}

    public Button getGetUserName() { return getUserName;}
    public void setGetUserName(Button getUserName) { this.getUserName = getUserName;}

    public Button getHelp() { return help;}
    public Button getHome() { return home;}
    public Button getPause() { return pause;}
    public Button getLoad() { return  load;}
    public Label getInvalid() { return  invalid;}

    public int getLevel() {return  level;}
    public void setLevel(int level) { this.level = level;}

    public void loadMenuScreen(){}
    public void loadLevelSelectScreen() {}
    public void loadGameplayScreen1() {}



}